import { Server } from "./Server";
const config = require('config-yml');
import express from 'express';


new Server(express(), config.app.retry, config.app.pause).startServer();