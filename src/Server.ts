import ResponseText from 'mitsi_bot_lib/dist/src/ResponseText';
import ServiceInput from 'mitsi_bot_lib/dist/src/ServiceInput';
import IntentDispatcherInput from 'mitsi_bot_lib/dist/src/IntentDispatcherInput';
import axios from 'axios';
import { path } from 'ramda';
import { Application, Response, Request } from "express";
import BaseService from 'mitsi_bot_lib/dist/src/BaseService';
const config = require('config-yml');


export class Server extends BaseService {

    private counters = new Map<string, number>();
    private retry: number;
    private pause: number;

    constructor(app: Application, retry: number, pause: number) {
        super(app);
        this.pause = pause;
        this.retry = retry;
    }

    onPost(request: Request, response: Response): void {
        const input = request.body as ServiceInput;
        const url = path(["parameters", "fields", "url", "stringValue"], input) as string | undefined;
        if (url) {
            this.checkSite(url);
            response.json(new ResponseText("Ok"));
            return;
        }
        response.json(new ResponseText(`Url malformed: ${url}`));
    }

    checkSite = (url: string) => {
        if (!this.counters.get(url)) {
            this.counters.set(url, 0);
        }
        if (this.counters.get(url)! >= this.retry) {
            this.sendNotification(url, false);
            return;
        }
        axios
            .get(url)
            .then(() => {
                console.log(`${url} available`);
                this.sendNotification(url, true);
            })
            .catch((err: Error) => {
                console.error(err)
                this.counters.set(url, this.counters.get(url)! + 1);
                setTimeout(this.checkSite, this.pause, url);
            });
    }

    private sendNotification(url: string, success: boolean) {
        const data = new IntentDispatcherInput("notification", false, `${url} is ${success ? "available" : "unavailable"}`);
        axios
            .post(config.webservices.intentDispatcher, data)
            .catch((err: NodeJS.ErrnoException) => {
                console.error("check availability", err);
            });
    }
}