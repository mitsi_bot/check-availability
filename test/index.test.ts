import ResponseText from 'mitsi_bot_lib/dist/src/ResponseText';
import IntentDispatcherInput from 'mitsi_bot_lib/dist/src/IntentDispatcherInput';
import { Server } from '../src/Server';
import axios from "axios";
import express, { Request, Response } from "express";
import ServiceInput from 'mitsi_bot_lib/dist/src/ServiceInput';
jest.mock('express');
jest.mock('axios');
const mockedAxios = axios as jest.Mocked<typeof axios>;
const mockedExpress = express as jest.Mocked<typeof express>;

describe("should respond on Post", () => {
    const server = new Server(mockedExpress.application, 50, 0);

    beforeEach(() => {
        mockedAxios.get.mockClear();
        mockedAxios.post.mockClear();
    });

    it("it should succeed send available", done => {
        mockedAxios.get.mockImplementationOnce((uri) => uri === "http://www.google.fr" ? Promise.resolve() : Promise.reject());
        mockedAxios.post.mockImplementationOnce((url, data: IntentDispatcherInput) => {
            expect(data.parameters).toBe("http://www.google.fr is available");
            expect(data.intent).toBe("notification");
            done();
            return Promise.resolve();
        });
        const request = {
            body: new ServiceInput({ fields: { url: { stringValue: "http://www.google.fr" } } }),
        } as Request;
        const response = { json: jest.fn((response: ResponseText) => {
            expect(response.displayText).toBe("Ok");
        }) } as any as Response;

        server.onPost(request, response);
        expect(mockedAxios.get).toHaveBeenCalledTimes(1);
        expect(response.json).toHaveBeenCalledTimes(1);
    });

    it("it should succeed send unavailable", done => {
        mockedAxios.get.mockImplementation((uri) => {
            if (uri === "http://www.google.fr") {
                return Promise.reject();
            }
            return Promise.resolve();
        });
        mockedAxios.post.mockImplementationOnce((url, data: IntentDispatcherInput) => {
            expect(data.parameters).toBe("http://www.google.fr is unavailable");
            expect(data.intent).toBe("notification");
            expect(mockedAxios.get).toHaveBeenCalledTimes(50)
            done();
            return Promise.resolve();
        });
        const request = {
            body: new ServiceInput({ fields: { url: { stringValue: "http://www.google.fr" } } }),
        } as Request;
        const response = { json: jest.fn((response: ResponseText) => {
            expect(response.displayText).toBe("Ok");
        }) } as any as Response;

        server.onPost(request, response);

        expect(mockedAxios.get).toHaveBeenCalledTimes(1);
        expect(response.json).toHaveBeenCalledTimes(1);
    });

    it("it should responde ulr malformed", done => {
        const request = {
            body: new ServiceInput({ fields: { url: undefined } }),
        } as Request;
        const response = { json: jest.fn((response: ResponseText) => {
            expect(response.displayText).toBe("Url malformed: undefined");
            done();
        }) } as any as Response;

        server.onPost(request, response);

        expect(response.json).toHaveBeenCalledTimes(1);
    });
});